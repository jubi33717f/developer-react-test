const config = {
  cognissAPI: 'http://localhost:3000',
  appName: 'Cogniss',
  defaultRoute: 'dashboard'
}
export default config
