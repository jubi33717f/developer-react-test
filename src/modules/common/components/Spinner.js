import React from 'react';
import { ClipLoader } from 'react-spinners';
import styled from 'styled-components'
import { withTheme } from 'styled-components'
const Wrapper = styled.div `
  display:inline-block;
`
const Spinner = ({size, color, theme, className})=>{
  size = size || 30
  color = color || theme.colors.dark
  return (
    <Wrapper className={className}>
      <ClipLoader color={theme.colors.dark} loading={true} size={size} />
    </Wrapper>
  )
}
export default withTheme(Spinner)
