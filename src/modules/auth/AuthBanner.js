import React from 'react'
import defaultAuthBanner from './auth-banner.png'
import styled from 'styled-components'
const Wrapper = styled.img`
  width:400px;
  margin-bottom:15px;
`
export default ()=>{
  return <Wrapper className='auth-banner' src={defaultAuthBanner} alt='banner'/>
}
