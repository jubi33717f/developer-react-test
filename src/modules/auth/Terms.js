import React,{useState} from 'react'
import styled from 'styled-components'
import CenteredScreen from '../layout/CenteredScreen'
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
const Wrapper = styled(CenteredScreen)`
   padding:0;
   margin:0;
  .body{
    background:#fff;
    z-index:1000;
  
    height:80vh;
    width:50vw;
    padding:0;
    margin:0;
    
  }
  .header{
      
      height:3.5em;
      background-color:#F1F1F1;
      width:100%;
      color:black;
      padding:0;
      h2{
          font-size:1.2em;
          line-height:3.5em;
      }
  }
  
  .content{
      overflow-y:scroll;
      padding:0;
      height:60vh;
      background:#fff;
      
      margin-top:2em;
      margin-left:2em;
      margin-right:2em;
      .statement{
          font-size:.9em;

          padding:.5em;
          padding-left:1.5em;
          padding-bottom:1em;
          background-color:#F1F1F1;
          border-radius:2em;
          text-align:left;
          line-height:.9em;
          font-weight:600;
      }
      .policy-box{
          width:99%;
          border:1px solid #F50357;
          text-align:left;

          p{
            margin-bottom:3em;
              
          }
         
      }
  }
  .footer{
      height:20vh;
      background:#fff;
      width:100%;
      .button-group{
          display:flex;
          width:100%;
          margin:auto;
          justify-content: center;
          button{
            border-radius:2em;
            height:2.5em;
            width:7em;
            margin:1em;
            outline:none;
          }
          button:nth-child(1){
            border:.15em solid #F50357;
            background-color:#fff;
            color:#F50357;
          }
          button:nth-child(2){
            border:.15em solid #FFF;
            background-color:#F50357;
            color:#FFF;

          }
      }
      span{
          font-size:1em;
      }
      
  }
`
function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`full-width-tabpanel-${index}`}
        aria-labelledby={`full-width-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }
    TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };
  
  function a11yProps(index) {
    return {
      id: `full-width-tab-${index}`,
      'aria-controls': `full-width-tabpanel-${index}`,
    };
  }
  
  const useStyles = makeStyles((theme) => ({
    root: {
      backgroundColor: theme.palette.background.paper,
      width: 500,
    },
  }));
const Terms = ({handleClose})=>{
    const classes = useStyles();
    const theme = useTheme();
    const [value, setValue] = useState(0);
    const [checked, setChecked] =useState(false)

    const handleCheck = ()=>{
        setChecked(!checked)
    }
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleChangeIndex = (index) => {
        setValue(index);
    };
    return (<Wrapper>
    <div className='header'>
        <h2>Terms & Privacy Policy</h2>
    </div>
    <div className='content'>
        <section>
            <div className='statement'><ErrorOutlineIcon style={{marginRight:'1em'}}/>Statement of intent</div>
        </section>
        <section>
            {/* <AppBar position="static" color="transparent" > */}
                <Tabs
                value={value}
                onChange={handleChange}
                indicatorColor="secondary"
                textColor="inherit"
                variant="standard"
                aria-label="full width tabs example"
                >
                    <Tab label="Terms of Use" {...a11yProps(0)} />
                    <Tab label="Privacy Policy" {...a11yProps(1)} />
                </Tabs>
            {/* </AppBar> */}
            <SwipeableViews
                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={value}
                onChangeIndex={handleChangeIndex}
            >
                <div className ='policy-box'>
                    <TabPanel value={value} index={0} dir={theme.direction}>
                    <h5 style={{marginTop:'1em'}}>What personal information we collect</h5>
                    <p>Depending on how you engage with Cogniss Services, we collect different kinds of information form or about you.</p>
                    <h5>Account and profile information</h5>
                    <p>We collect information about you when you register for an account, create or modify your profile, set preference, or signup for our Services. This includes your name, email address, password, and in some cases your date of birth and gender.</p>
                    <h5>Information you provide to us</h5>
                    <p>We collect and store any content that you create, post send, and share to our Services. This content includes any comments you post, emails and other communications that you send to us, and information you provide by responding to surveys, submitting a form or participating in promotions. We also track how you engage with message you receive in connection with our Services, such as notifications to complete a learning activity</p>
                    <h5>Account and profile information</h5>
                    <p>We collect information about you when you register for an account, create or modify your profile, set preference, or signup for our Services. This includes your name, email address, password, and in some cases your date of birth and gender.</p>
                    <h5>Information you provide to us</h5>
                    <p>We collect and store any content that you create, post send, and share to our Services. This content includes any comments you post, emails and other communications that you send to us, and information you provide by responding to surveys, submitting a form or participating in promotions. We also track how you engage with message you receive in connection with our Services, such as notifications to complete a learning activity</p>
                    </TabPanel>
                    <TabPanel value={value} index={1} dir={theme.direction}>
                    Item Two
                    </TabPanel>
                </div>
            </SwipeableViews>
        </section>
    </div>
    <div className='footer'>
        <FormControlLabel
            control={<Checkbox checked={checked} onChange={handleCheck} name="checked" />}
            label="I have read and agree to the Terms and Privacy Policy"
        />
        <div className='button-group'>
            <button onClick={handleClose}>Cancel</button>
            {checked?<button  onClick={handleClose}>Accept</button>:<button disabled style={{backgroundColor:'#AEBFCD'}}>Accept</button>}
        </div>
    </div>


</Wrapper>)
}


export default Terms