import React,{useState} from 'react';
import {Link} from 'react-router-dom'
import {authRequirements} from '../common/constants'
import Title from '../common/components/Title'
import Input from '../common/hookforms/Input'
import Email from '../common/hookforms/Email'
import Submit from '../common/hookforms/Submit'
import Password from '../common/hookforms/Password'
import CenteredScreen from '../layout/CenteredScreen'
import {Form} from 'react-bootstrap'
import useForm from 'react-hook-form'
import AuthBanner from './AuthBanner'
import styled from 'styled-components'
import Terms from './Terms'
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
const Wrapper = styled(CenteredScreen)`
  .auth-banner{
    width:325px;
    margin-bottom:15px;
    @media (min-width: 768px) {
        width:400px;
    }
  }
  form{
    width:300px;
    text-align:left;
    margin:0 auto 15px;
    .submit{
      margin-top:42px;
      text-align:center;
    }
  }
`
const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    opacity:1,
    // visibility: visible,
  },
}));
const Registration = ({location, history})=> {
    
  const form = useForm()
  const onSubmit = (val) =>{
    handleOpen()
  }
  const onlyAlpha = /^\w+$/
  const [open, setOpen] = useState(false);
  const classes = useStyles();
  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    console.log('c')
  };
  
  

  return (<><Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
            timeout: 500,
            }}
        >
            <Fade in={open}>
            <div style={{opacity:'1',visibility:'visible',padding:'0'}} className={classes.paper}>
                <Terms handleClose={handleClose}/>
            </div>
            </Fade>
        </Modal>
    <Wrapper className="screen-login">
    <Title title="Log in"/>
    <div>aas</div>
      
    <div className="container-fluid">
      <AuthBanner/>
      
      <Form horizontal onSubmit={form.handleSubmit(onSubmit)}>
        <Input name="username" form={form} label="Username"
        minLength={authRequirements.USERNAME_MIN} maxLength={authRequirements.USERNAME_MAX}
        pattern={onlyAlpha} patternLabel='Must be only a-z 0-9' required/>
        <Email name="email" form={form} label="Email" required/>
        <Password name="password" form={form} label="Password" required/>
        <p>By clicking Create Account you agree to our <Link to="/terms">Terms</Link> and <Link to="/privacy">Privacy Policy</Link></p>
        <Submit className="submit" label="Create account" form={form} primary />
        
      </Form>
      <p>Already have an account? <Link to="login">Sign in now</Link></p>
    </div>
    
  </Wrapper></>)
}
export default Registration
