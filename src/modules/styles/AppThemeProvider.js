import React from 'react'
import defaultTheme from './theme'
import styled, {ThemeProvider} from 'styled-components'
import { withRouter } from 'react-router-dom'
const Wrapper = styled.div`
  height:100%;
  display:flex;
  flex-direction:column;
`
export default withRouter(({children, location})=>{
  return (<ThemeProvider theme={defaultTheme}>
    <Wrapper>
      {children}
    </Wrapper>
  </ThemeProvider>)
})
